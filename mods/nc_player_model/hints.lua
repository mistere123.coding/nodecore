-- LUALOCALS < ---------------------------------------------------------
local nodecore
    = nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.register_hint("go for a swim",
	{true,
		"anim_swim_up",
		"anim_swim_down",
		"anim_swim_mine"
	}
)
